<?php

namespace laratube\Http\Controllers;

use Illuminate\Http\Request;
use laratube\Http\Requests\Videos\UpdateVideoRequest;
use laratube\Video;

class VideoController extends Controller
{
    public function show (Video $video)
    {
        if(request()->wantsJson())
        {
            return $video;
        }

        return view('video',compact('video'));
    }

    public function updateViews(Video $video)
    {
        $video->increment('views');
        return response()->json([]);
    }

    public function update(UpdateVideoRequest $request,Video $video){
        if($request)
            $video->update($request->only(['title','description']));
        else
            return redirect()->to('google.com');
    }
}
