<?php

namespace laratube;



class Vote extends Model
{
    public function voteable()
    {
        return $this->morphTo();
    }
}
