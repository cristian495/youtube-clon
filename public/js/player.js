var player = videojs('video');

var viewLogged = false;
player.on('timeupdate',function(){
    var percentagePlayer = Math.ceil(player.currentTime()/ player.duration() * 100);


    if(percentagePlayer > 10  && !viewLogged ){
        axios.put('/videos/'+window.CURRENT_VIDEO);
        console.log(percentagePlayer)
        viewLogged = true;
    }
});